const path = require('path');

module.exports = {
    mode: 'production',
    entry:

        './src/index.js'
    ,
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env', '@babel/preset-react']
                }
            },
            {
                test: /\.css/,
                //Here is where I removed the property
                loaders: ['style-loader', 'css-loader']
            },
        ],
    }
};
