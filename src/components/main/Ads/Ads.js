import React, {Component} from 'react';
import OneAd from '../AdOne/AdOne';
import './ads.css'

export default class Ads extends Component {
    render() {
        const {deleteAd, todos} = this.props;
        const elements = todos.map((item, i) => {
            const {...itemProps} = item;
            return (
                <div key={i}>
                    <OneAd
                        {...itemProps}
                        index={i}
                        deleteAd={deleteAd}/>
            </div>
            )
        }).reverse();

        return(
                <ul className='ads' >
                    { elements }
                </ul>
        )
    }
}