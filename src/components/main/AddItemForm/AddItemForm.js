import React, { Component } from 'react';
import './add-item-form.css';

export default class AddForm extends Component {
    state = {
        label: '',
        price: '',
        descriptions: '',
        contacts: '',
        classification: ''
    };

    change = (e) => {
        const {
            name,
            value
        } = e.target;
        this.setState({
            [name]: value
        })
    };
    onSubmit = (e) => {
        const { label, price, classification, descriptions, contacts } = this.state;
        e.preventDefault();
        this.props.addItem(label, price, classification, descriptions, contacts);
        this.setState( {
            label: '',
            price: '',
            descriptions: '',
            contacts: ''
        })
    };
    render() {
        const { label, price, descriptions, contacts, classification } = this.state;
        return (
            <form className='add-form'
                  onSubmit={this.onSubmit}>
                <p align='center'><input required type="text"
                       maxLength='40'
                       className="form-name"
                       placeholder="Название"
                       name="label"
                       value={label}
                       onChange={this.change}/></p>
                <p align='center'><input required type="text"
                       className="form-price"
                       placeholder="Цена(в грн)"
                       name="price"
                       value={price}
                       onChange={this.change}/></p>
                <p align='center'><select name="classification" value={classification} onChange={this.change} required>
                    <option>Тип товара</option>
                    <option value='clothes'>Одежда</option>
                    <option value='techno'>Техника</option>
                    <option value='games'>Игры</option>
                </select></p>
                <textarea type="text"
                          className="form-desc"
                          placeholder="Описание"
                          name="descriptions"
                          value={descriptions}
                          onChange={this.change}/>
                <p align='center'><input required type="text"
                          className="form-contacts"
                          placeholder="Телефон"
                          name="contacts"
                          value={contacts}
                          onChange={this.change}/></p>
                <p align='center'><button>
                    Добавить обьявление
                </button></p>
            </form>
        )
    }
}
