import React, { Component } from "react";
import './item-classification-filter.css'

export default class ItemClassificationFilter extends Component {

    buttons = [
        {name: 'all', label: 'Все'},
        {name: 'clothes', label: 'Одежда'},
        {name: 'techno', label: 'Техника'},
        {name: 'games', label: 'Игры'},
    ];

    render() {
        const { onFilterChange } = this.props;

        const buttons = this.buttons.map(({  label, name }) => {
            return (
                <ul key={name}>
                    <li>
                        <button type="button"
                                className="btn-classification"
                                onClick={() => onFilterChange(name)}>
                            { label }
                        </button>
                    </li>
                </ul>
            )
        });
        return (
            <div className='list-classification'>
                <h3>Классификация</h3>
                { buttons }
            </div>
        )
    }
}
