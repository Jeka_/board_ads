import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Name from './Name.js';
import SearchPanel from './SearchPanel';
import './app-header.css'


export default class AppHeader extends Component {
    render() {
        const {onSearchChange} = this.props;
        return (
            <div className='app-header'>
                <div className='name'>
                    <Link to='/'>
                        <Name />
                    </Link>
                </div>
                <div className='search'>
                    <SearchPanel onSearchChange={onSearchChange}/>
                </div>
            </div>
        );
    }
}