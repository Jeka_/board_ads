import React, { Component } from 'react';
import ItemClassificationFilter from '../ItemClassificationFilter/ItemClassificationFilter.js';
import Ads from '../Ads/Ads';
import AddForm from "../AddItemForm/AddItemForm";
import './app-body.css'

export default class AppBody extends Component {


    render() {
        const {ads, addItem, deleteAd, todos, onFilterChange} = this.props;
        return (
                <div className='app-body'>
                    <ItemClassificationFilter onFilterChange={onFilterChange}/>
                    <Ads ads={ads} deleteAd={deleteAd} todos={todos}/>
                    <AddForm addItem={addItem}/>
                </div>
        );
    }
}