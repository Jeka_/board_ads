import React, {Component} from "react";
import {Link} from "react-router-dom";
import './ad-one.css';

export default class OneAd extends Component {

    render() {
        const {label, price, date, index} = this.props;
        return (

                    <div className='ad-one' align='center'>
                        <Link to={`/ad/${index}`}>
                        <img src=""/>
                        <p>{label}</p>
                        <h3>{price} грн</h3>
                        <p>Добавлено {date}</p>
                        </Link>
                    </div>

        )
    }
}

