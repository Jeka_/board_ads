import React, { Component } from "react";
import {Link} from "react-router-dom";
import "./ad-one-new-window.css";


export default class AdvertDetails extends Component {
    render() {
        const { itemId, deleteAd, todos} = this.props;
        return (
            <div className="ad-one-new-window">
                <div className="ad-one-new-window-grid">
                    <img src=""/>
                    <div>
                        <h1 align='center'>{todos[itemId].label}</h1>
                        <h2 align='center'>{todos[itemId].price} грн</h2>
                        <p>Контакты: {todos[itemId].contacts}</p>
                    </div>
                </div>
                <p>Описание: {todos[itemId].descriptions}</p>
                    <Link to='/'>
                        <button type='button'
                                onClick={() => deleteAd(itemId)}>
                            Удалить обьявление
                        </button>
                    </Link>
            </div>
        )
    }

}

