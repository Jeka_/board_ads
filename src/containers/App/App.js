import React, { Component } from 'react';
import moment from "moment";
import {BrowserRouter, Route } from "react-router-dom";
import AppHeader from '../../components/main/Header/AppHeader';
import AppBody from "../../components/main/AppBody/AppBody";
import AdvertDetails from "../pages/AdvertDetails/AdvertDetails";


export default class App extends Component {
    numberAd = 1;
    state = {
        ads: [
            this.createAd("Minecraft", "50", 'games',"", 380677449265),
            this.createAd("Телефон", "10 000", 'techno',"", 380677409265),
            this.createAd("Машина", "150 000", 'techno',"", 380677409237)
        ],
        term: '',
        filter: 'all',
    };


    addItem = (label, price, descriptions, contacts) => {
        const newAd = this.createAd(label, price, descriptions, contacts);

        const { ads } = this.state;

        this.setState({
            ads: [...ads, newAd],
        });
    };
    deleteAd = (i) => {
        const newArr = this.state.ads;
        newArr.splice(i, 1);
        this.setState({
            ads: newArr
        });
    };

    createAd(label, price, classification, descriptions, contacts) {
        return {
            label,
            price,
            classification,
            descriptions,
            contacts,
            date: moment().format('D.0M.YYYY в H:m'),
            number: this.numberAd++,
        }
    }
    onSearchChange = (term) => {
        this.setState({ term });
    };
    onFilterChange = (filter) => {
        this.setState({ filter });
    };
    search(items, term) {
        if(term.length === 0) {
            return items;
        }
        return items.filter((item) => {
            return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1;
        })
    }
    filter(items, filter) {
        switch(filter) {
            case 'all':
                return items;
            case 'clothes':
                return items.filter((item) => item.classification==='clothes');
            case 'techno':
                return items.filter((item) => item.classification==='techno');
            case 'games':
                return items.filter((item) => item.classification==='games');
            default:
                return items;
        }
    }

    render() {

        const { ads, term, filter } = this.state;
        const searchAd = this.search(ads, term);
        const visibleAds = this.filter(searchAd, filter);

        return (
            <div className='app-main'>
                <BrowserRouter>
                    <Route path="/" render={() => <AppHeader onSearchChange={this.onSearchChange }/>}/>
                    <Route exact path="/" render={() => <AppBody
                        ads={ads}
                        addItem={this.addItem}
                        deleteAd={this.deleteAd}
                        todos={visibleAds}
                        onFilterChange={this.onFilterChange}/>}/>
                    <Route path="/ad/:id"
                            render={({match}) => {
                                const {id} = match.params;
                                return <AdvertDetails itemId={id} ads={ads} todos={visibleAds} deleteAd={this.deleteAd}/>
                            }}/>
                </BrowserRouter>
            </div>
        );
    }
}
